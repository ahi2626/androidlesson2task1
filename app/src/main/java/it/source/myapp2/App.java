package it.source.myapp2;

import android.app.Application;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //before activity created
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        //after activity destroyed
    }

    public void info() {}

}