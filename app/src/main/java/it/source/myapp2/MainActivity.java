package it.source.myapp2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayDeque;

import androidx.appcompat.app.AppCompatActivity;

import java.text.CollationElementIterator;

public class MainActivity extends AppCompatActivity  implements OnClickListener {

    EditText textEditor;
    Button btnSave;
    Button btnLoad;

    SharedPreferences sPref;

    final String SAVED_TEXT = "saved_text";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textEditor = (EditText) findViewById(R.id.textEditor);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener((View.OnClickListener) this);

        btnLoad = (Button) findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener((View.OnClickListener) this);

        loadText();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                saveText();
                break;
            case R.id.btnLoad:
                loadText();
                break;
            default:
                break;
        }
    }

    void saveText() {
        sPref = getPreferences(MODE_PRIVATE);
        Editor ed = sPref.edit();
        ed.putString(SAVED_TEXT, textEditor.getText().toString());
        ed.commit();
        Toast.makeText(this, "Text saved", Toast.LENGTH_SHORT).show();
    }

    void loadText() {
        sPref = getPreferences(MODE_PRIVATE);
        String savedText = sPref.getString(SAVED_TEXT, "");
        textEditor.setText(savedText);
        Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveText();
    }

}







//@Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//
//        final ArrayDeque<String> inputList = new ArrayDeque<String>();
//          buttonBack = findViewById(R.id.buttonBack);
//          buttonDelete = findViewById(R.id.buttonDelete);
//           textEditor = findViewById(R.id.textEditor);
//
//        buttonDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CollationElementIterator EditText;
//                textEditor.setText("");
//            }
//        });
//
//
//  buttonBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                textEditor.setText(inputList.pollLast());
//            }
//        });



//1. Создать приложение с одним экраном.готово
// Добавить на экран поле для ввода текста (EditText) и две кнопки.  готово
// По нажатию на первую кнопку с текстового поля весь введенный текст исчезает. готово
// По нажатию на вторую кнопку поле ввода восстанавливает текст, удаленный первой кнопкой.
//1.1* По повторному нажатию на вторую кнопку, текстовое поле восстанавливает текст, введенный ранее.
// (Например: введено слово "мышь" -> поле очищено -> введено слово "хомяк" -> поле очищено ->
// нажата восстанавливающая кнопка, появился текст "хомяк" -> нажата
//восстанавливающая кнопка еще раз -> появился текст "мышь").
// Реализовать сохранение/восстановление пяти состояний.
